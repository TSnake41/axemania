local Music = require "lib.base":extend()

function Music:constructor(path)
  self.source = love.audio.newSource(path, "stream")
end

function Music:add()
  print(self.source)
  love.audio.play(self.source)
end

function Music:update(dt, vars, components, pool)
  self.source:setPitch(vars.td)
end

return Music
