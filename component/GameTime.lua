local flux = require "lib.flux"

local GameTime = {}
local group = flux.group()

function GameTime:init(vars, pool, components)
  self.vars = vars
end

function GameTime:update(dt, vars, pool, components)
  group:update(dt)

  vars.dt = dt * vars.td
end

function GameTime:slowdown()
  group:to(self.vars, 2, { td = 0.25 }):ease("expoout")
    :after(self.vars, 5, { td = 1 }):ease("expoin")
end

return GameTime
