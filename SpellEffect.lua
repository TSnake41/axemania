local SpellEffect = require "lib.base":extend()
local CircleEffect = require "CircleEffect"

local default_params = {
  begin = 5,
  step = 3,
  speed = 10,
  count = 2,
  ccount = 5,
  part = .5,
  color = { 1, 1, 1, 1 },
  offset = { .5, .5 }
}

function SpellEffect:constructor(entity, params)
  self.entity = entity
  self.circles = {}

  if params then
    for k,v in pairs(default_params) do
      if not params[k] then
        params[k] = default_params[k]
      end
    end
  else
    params = default_params
  end

--pool:queue(CircleEffect({ x = 0, y = 0 }, 20 + i * 10, nil, 10, 2, nil, -i*.5))
--function CircleEffect:constructor(center, radius, color, speed, count, part, delta)

  for i=0,params.ccount do
    self.circles[i] = CircleEffect(
      entity,
      params.begin + i * params.step,
      params.color,
      params.speed,
      params.count,
      params.part,
      -i*.5
    )
  end
end

function SpellEffect:update(dt)
  for i,v in ipairs(self.circles) do
    v:update(dt)
  end
end

function SpellEffect:draw()
  for i,v in ipairs(self.circles) do
    v:draw()
  end
end

return SpellEffect
