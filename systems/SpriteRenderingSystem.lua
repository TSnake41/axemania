-- This is a generic quad that contains the entire texture.
local unit_quad = love.graphics.newQuad(0, 0, 1, 1, 1, 1)

return {
  filter = { "sprite" },
  process = {
    draw = function (renderer)
      for _, self in ipairs(renderer.entities) do
        local quad, anim = unit_quad, self.animation

        if anim then
          -- take care of animations
          quad = anim.model[anim.param or 1][anim.tick + 1]
        end

        love.graphics.draw(self.sprite, quad, self.x, self.y, 0, 1, 1,
          self.w/2, self.h/2)
      end
    end
  }, sort = function (a, b)
    local az, bz = a.z or 0, b.z or 0
    return az < bz
  end
}
