local function animate_entity(entity, dt)
  local animation = entity.animation

  local model = animation.model or animation.default
  if not model.ft or animation.stop then
    -- static animation
    return
  end

  local delta = (animation.delta or 0) + dt
  
  while delta > model.delta_max do
    delta = delta - model.delta_max
  
    if not model.loop then
      model = model.next or animation.default
      animation.model = model
      
      if not model.ft then
        animation.delta = 0
        animation.tick = 0
        return
      end
    end
  end

  animation.model = model
  animation.delta = delta
  animation.tick = math.floor(delta / model.ft)
end

return {
  filter = { "animation" },
  process = {
    update = function (self, dt)
      for _, entity in ipairs(self.entities) do
        animate_entity(entity, dt)
      end
    end
  }
}
