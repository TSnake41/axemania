local CircleEffect = require "lib.base":extend()

function CircleEffect:constructor(center, radius, color, speed, count, part, delta)
  self.center = center

  self.radius = radius or 5
  self.color = color or { 1, 1, 1, 1 }
  self.speed = speed or 1

  self.count = count or 3
  self.part = part or .5

  self.time = delta or 0
end

function CircleEffect:update(dt)
  self.time = (self.time + dt * self.speed) % (math.pi * 2)
end

function CircleEffect:draw()
  local cx, cy = self.center.x, self.center.y

  local offset = (self.part * math.pi * 2) / self.count
  local angle = ((1 - self.part) * math.pi * 2) / self.count

  local r, g, b, a = love.graphics.getColor()
  love.graphics.setColor(self.color)

  for i=0,self.count-1 do
    local alpha = self.time + i * (angle + offset)
    love.graphics.arc("line", "open", cx, cy, self.radius, alpha - offset, alpha)
  end
  love.graphics.setColor(r, g, b, a)
end

function CircleEffect:keypressed(key)
  if key == 'a' then
    self.count = self.count + 1
  end
  if key == 'q' then
    self.count = self.count - 1
  end

  if key == 'z' then
    self.part = math.min(self.part + .05, 0.95)
  end
  if key == 's' then
    self.part = math.max(self.part - .05, 0.05)
  end

end

return CircleEffect
