return function (img)
  local animations = {
    idle = { range = { 0, 0 } },
    walk = { range = { 2, 9 }, loop = true, ft = .15 },
    attack = { range = { 10, 18 }, ft = .1 },
    charge = { range = { 19, 21 }, ft = .25, next = "attack" },
    dying = { range = { 22, 28 }, ft = .1, next = "dead" },
    dead = { range = { 28, 28 } }
  }

  for name,anim in pairs(animations) do
    local range = anim.range
    anim.range = nil

    local r_start = range[1]
    local r_end = range[2]

    anim.duration = r_end - r_start
    anim.name = name

    if anim.ft then
      anim.delta_max = anim.ft * anim.duration
    end

    for i=1,8 do
      anim[i] = {}

      for j=1,anim.duration+1 do
        anim[i][j] = love.graphics.newQuad (
          (j - 1 + r_start) * 32,
          (i - 1) * 32,
          32, 32,
          img:getDimensions()
        )
      end
    end

    if anim.next then
      anim.next = animations[anim.next]
    end
  end

  return animations
end
