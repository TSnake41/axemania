local InputController = require "lib.base":extend()

function InputController:constructor()
  self.data = {
    movement = {}
  }
end

function InputController:update(entity, dt, vars, components, pool)
  local data = self.data

  -- find player
  local player
  for _,v in pairs(pool.entities) do
    if v.team == 0 then
      player = v
      break
    end
  end

  data.movement[1] = 0
  data.movement[2] = 0
  data.slow = false
  data.attack = false

  if player then
  end

  return data
end

function InputController:debug()
end

return InputController
