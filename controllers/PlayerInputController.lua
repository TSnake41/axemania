local InputController = require "lib.base":extend()

function InputController:constructor(io)
  self.io = io
  self.data = {
    movement = {}
  }
end

function InputController:update(entity, dt)
  self.io:update()
  local data = self.data

  data.movement[1], data.movement[2] = self.io:get "move"
  data.slow = self.io:pressed "slow"
  data.attack = self.io:pressed "attack"

  return data
end

function InputController:debug()
  return {
    "debug test"
  }
end

return InputController
