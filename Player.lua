local Player = require "lib.base":extend()

local directions = {
  down = 0,
  left = 1,
  right = 2,
  up = 3,
  down_left = 4,
  down_right = 5,
  up_left = 6,
  up_right = 7
}

function Player:constructor(x, y, sprite, anim_db, controller)
  self.x = x
  self.y = y
  self.z = 1
  self.direction = 1
  self.player = true

  self.speed = 64

  self.sprite = sprite
  self.anim_db = anim_db

  self.w, self.h = 32, 32

  self.animation = {
    default = anim_db.idle,
    model = anim_db.idle,
    tick = 0,
    param = 2
  }

  self.team = 0

  self.controller = controller
end

function Player:update(dt, vars, components, pool)
  local controller = self.controller

  if not self.controller then
    return
  end

  local input = controller:update(self, dt, vars, components, pool)

  local x, y = unpack(input.movement)
  x = math.max(math.min(1, x), -1)
  y = math.max(math.min(1, y), -1)

  local anim = self.animation

  if input.attack then
    if anim.model.name == "idle" or anim.model.name == "walk" then
      anim.model = self.anim_db.charge
      anim.tick = 0
      anim.delta = 0
    end
  end

  if input.slow then
    components.GameTime:slowdown()
  end

  if anim.model.name ~= "attack" then
    local dx, dy
    if x ~= 0 then
      dx = (x < 0) and "left" or "right"
    end

    if y ~= 0 then
      dy = (y < 0) and "up" or "down"
    end

    if dx or dy then
      if anim.model.name == "idle" then
        -- just... walk
        anim.model = self.anim_db.walk
      end

      self.direction = directions [
        (dx and dy) and string.format("%s_%s", dy, dx) or (dx or dy)
      ]

      anim.param = self.direction + 1
    else
      if anim.model.name == "walk" then
        anim.model = self.anim_db.idle
        anim.tick = 0
        anim.delta = 0
      end
    end
  end

  self.x = self.x + x * dt * self.speed
  self.y = self.y + y * dt * self.speed

end

return Player
