local function dummy() end

return {
  setup = dummy,
  start = dummy,
  finish = dummy
}