local nata = require "lib.nata"
local baton = require "lib.baton"

local SpellEffect = require "SpellEffect"
local PlayerEntity = require "Player"
local Music = require "Music"

local PlayerController = require "controllers.PlayerInputController"

local vars = { td = 1 }
local pool = nata.new {
  require "systems.AnimationSystem",
  nata.oop,
  require "systems.SpriteRenderingSystem",
}

local components = {
  GameTime = require "component.GameTime"
}

local maid64 = require "lib.maid64"

local input_controller = PlayerController(baton.new {
  controls = {
    left = { "key:left", "axis:leftx-", "button:dpleft" },
    right = { "key:right", "axis:leftx+", "button:dpright" },
    up = { "key:up", "axis:lefty-", "button:dpup" },
    down = { "key:down", "axis:lefty+", "button:dpdown" },
    reset = { "key:a", "button:b" },
    attack = { "key:e", "button:a" },
    slow = { "key:s", "button:x" },
    quit = { "key:escape", "button:start" }
  },
  pairs = {
    move = {"left", "right", "up", "down"}
  },
  joystick = love.joystick.getJoysticks()[1],
  deadzone = .33,
})

function love.load()
  maid64.setup(640, 360)

  local player_spritesheet = {
    love.graphics.newImage "assets/axeman.png",
    love.graphics.newImage "assets/axeman-dark.png",
    love.graphics.newImage "assets/axeman-black.png",
    love.graphics.newImage "assets/axeman-blur.png",
  }

  for _,sprite in pairs(player_spritesheet) do
    sprite:setFilter("nearest")
  end

  local player_anim_db = (require "PlayerAnimations")(player_spritesheet[1])
  local player = PlayerEntity(64, 64, player_spritesheet[1], player_anim_db, input_controller)

  local spell = SpellEffect(player)
  pool:queue(spell)

  local random_io = require "controllers.RandomInputController"

  local player_tests = {}
  for i=4,20 do
    for j=6,16 do
      if not (i % 2 == 1 or j % 2 == 1) then
        local sprite = player_spritesheet[math.random(1,#player_spritesheet)]
        local p = PlayerEntity(i * 16, j * 16, sprite, player_anim_db, random_io())
        p.animation.model = player_anim_db.charge
        p.animation.default = player_anim_db.charge
        p.animation.delta = math.random() * 2
        p.team = 1

        pool:queue(p)
      end
    end
  end

  pool:queue(player)
  pool:queue(Music "assets/music.ogg")
  pool:flush()

  for n,v in pairs(components) do
    v:init(vars, components, pool)
  end
end

function love.update(dt)
    love.window.setTitle("FPS: " .. love.timer.getFPS()
    .. " | Memory: " .. math.floor(collectgarbage 'count') .. 'kb')

  for n,v in pairs(components) do
    v:update(dt, vars, components, pool)
  end

  pool:process("update", vars.dt, vars, components, pool)
end

--[[
local function anim_to_string(anim)
  local model = anim.model

  if (anim.delta or 0) == 0 then
    return model.name
  end

  return string.format("%s frame:%02d delta:%3.3f%s", model.name, anim.tick, anim.delta, model.loop and "L" or "")
end
]]

function love.draw()
  love.graphics.setBackgroundColor(.3, .5, 1, 1)

  maid64.start()
  love.graphics.setLineWidth(1)

  pool:process("draw")
  maid64.finish()

  --love.graphics.print(anim_to_string(player.animation), 0, 0)
  --love.graphics.print(string.format("(%g;%g)",player_input:get 'move'), 0, 16)
  --love.graphics.print(string.format("td:%g", vars.td), 0,0)
end

function love.gamepadpressed(device, key)
  pool:process("keypressed", key, scancode, isrepeat)
end
